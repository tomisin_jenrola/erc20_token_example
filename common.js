import Web3 from 'aion-web3'
import chai from 'chai'
import unlock from './utils/unlock.js'
import compile from './utils/compile.js'
import deploy from './utils/deploy.js'
// module.exports.provider = "http://localhost:8545";
export const provider = 'http://23.101.119.41:8545'
global.web3 = new Web3(new Web3.providers.HttpProvider(provider))

export const mainAccount = web3.personal.listAccounts[0]
export const mainAccountPass = 'PLAT4life'
export const secondaryAccounts = web3.personal.listAccounts.filter(
  (acc) => acc !== mainAccount
)

export const sleepTime = 10000
export const contractAddress = ''
// global.assert = require("chai").assert
global.expect = chai.expect
// global.BigNumber = require('bignumber.js');
global.unlock = unlock
global.compile = compile
global.deploy = deploy
