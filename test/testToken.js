import fs from 'fs'
import { mainAccount, secondaryAccounts, mainAccountPass } from '../common.js'

const sol = fs.readFileSync(__dirname + '/contracts/TestToken.sol', {
  encoding: 'utf8'
})

let acc1 = mainAccount
let acc2 = secondaryAccounts[0]
let acc3 = secondaryAccounts[1]
let pwd = mainAccountPass

let abi
let code
let contractAddr
let contractInstance

const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

describe('a] Compile and deploy', () => {
  it('should compile contract', async () => {
    const { TestToken } = await compile(web3, sol)
    abi = TestToken.info.abiDefinition
    code = TestToken.code
    expect(abi).to.not.be.null

    expect(abi).to.be.an('array')
    expect(code).to.not.be.null
    expect(code).to.be.an('string')
  }).timeout(0)

  it('should deploy contract', async () => {
    await unlock(web3, acc1, pwd)
    await unlock(web3, acc2, pwd)
    const { address } = await deploy(web3, acc1, abi, code)
    contractAddr = address
    contractInstance = await web3.eth.contract(abi).at(contractAddr)

    expect(contractAddr).to.have.lengthOf(66)
  }).timeout(0)
})

describe('b] Test token functions', () => {
  it('should get the total supply', async () => {
    const supply = await parseInt(
      contractInstance.totalSupply
        .call({ from: acc1, gas: 1500000 })
        .toString(10),
      10
    )

    expect(supply).to.equal(10000000)
  }).timeout(0)

  it('should get the right balance of acc1', async () => {
    const balance = await parseInt(
      contractInstance.balanceOf
        .call(acc1, { from: acc1, gas: 1500000 })
        .toString(10),
      10
    )

    expect(balance).to.equal(10000000)
  }).timeout(0)

  it('should transfer to acc2 successfully', async () => {
    const txHash = await contractInstance.transfer(acc2, 200, {
      from: acc1,
      gas: 1500000
    })

    await sleep(30e3)

    const txReceipt = web3.eth.getTransactionReceipt(txHash)

    // console.log(txReceipt['status'])
    expect(txReceipt).to.not.be.null
  }).timeout(0)

  it('should get the right balance of acc2', async () => {
    const balance = await parseInt(
      contractInstance.balanceOf
        .call(acc2, { from: acc2, gas: 1500000 })
        .toString(10),
      10
    )
    expect(balance).to.equal(200)
  }).timeout(0)

  it('should transfer to acc3 on behalf of acc1 successfully', async () => {
    const txHash = await contractInstance.transferFrom(acc1, acc3, 20, {
      from: acc2,
      gas: 1500000
    })

    await sleep(40e3)

    const txReceipt = web3.eth.getTransactionReceipt(txHash)

    // console.log(txReceipt['status'])
    expect(txReceipt).to.not.be.null
  }).timeout(0)

  it('should get the right balance of acc3', async () => {
    const balance = await parseInt(
      contractInstance.balanceOf
        .call(acc3, { from: acc3, gas: 1500000 })
        .toString(10),
      10
    )
    expect(balance).to.equal(20)
  }).timeout(0)
})
