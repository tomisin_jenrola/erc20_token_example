module.exports = function(w3, acc0, abi, code) {
  return new Promise((resolve, reject) => {
    console.log('deploying')
    w3.eth.contract(abi).new(
      {
        from: acc0,
        data: code,
        gas: 4700000
      },
      (err, contract) => {
        if (err) {
          // console.log("rejecting...")
          reject(err)
        } else if (contract && contract.address) {
          // console.log("resolving...")
          resolve(contract)
        }
      }
    )
  })
}
